/// <reference types="node" />
/// <reference types="serialport" />
import * as SerialPort from 'serialport';
import { AsyncQueue } from './utils';
export declare class UartProtocol implements AsyncIterable<Buffer> {
    private writeCb;
    private _send_seq;
    private _rec_seq;
    private _buffer;
    private _reset_deferred;
    private _pending;
    private _sendq;
    private _readq;
    private _nextIterator;
    private _dataFrameReceived;
    private _reset_timeout;
    _connected_future: Function | undefined;
    _transport: SerialPort;
    constructor(writeCb: (data: Buffer) => Promise<void>);
    data_received(data: Buffer): void;
    _extract_frame(data: Buffer): (Buffer | null)[];
    frame_received(data: Buffer): void;
    data_frame_received(data: Buffer): void;
    [Symbol.asyncIterator](): AsyncQueue<Buffer>;
    ack_frame_received(data: Buffer): void;
    nak_frame_received(data: Buffer): void;
    rst_frame_received(data: Buffer): void;
    rstack_frame_received(data: Buffer): void;
    error_frame_received(data: Buffer): void;
    write(data: Buffer): Promise<void>;
    reset(): Promise<any>;
    private sleep(ms);
    _send_task(): Promise<void>;
    _handle_ack(control: number): void;
    _handle_nak(control: number): void;
    data(data: Buffer): void;
    _data_frame(data: Buffer, seq: number, rxmit: number): Buffer;
    _ack_frame(): Buffer;
    _rst_frame(): Buffer;
    _frame(control: ArrayLike<number>, data?: ArrayLike<number>): Buffer;
    _randomize(s: Buffer): Buffer;
    _stuff(s: Iterable<number>): Buffer;
    _unstuff(s: Buffer): Buffer;
    static connect(portAddress: string, connectionOptions: {}): Promise<[UartProtocol, SerialPort]>;
}
