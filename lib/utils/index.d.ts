export declare class Deferred<T> {
    promise: Promise<T>;
    _resolve: Function;
    _reject: Function;
    _isResolved: boolean;
    _isRejected: boolean;
    constructor();
    resolve(value: T): void;
    reject(value: T): void;
    readonly isResolved: boolean;
    readonly isRejected: boolean;
    readonly isFullfilled: boolean;
}
export declare class AsyncQueue<T> {
    private queue;
    private waiting;
    constructor(initializer: Function);
    next(): Promise<IteratorResult<T>>;
    [Symbol.asyncIterator]: () => this;
}
