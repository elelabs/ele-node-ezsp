"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
if (typeof (Symbol as any).asyncIterator  === 'undefined') {
 (Symbol as any).asyncIterator = Symbol.asyncIterator || Symbol('asyncIterator');
}
// Symbol.asyncIterator = Symbol.asyncIterator || Symbol.for("Symbol.asyncIterator");
class Deferred {
    constructor() {
        this._isResolved = false;
        this._isRejected = false;
        this.promise = new Promise((resolve, reject) => {
            this._resolve = resolve;
            this._reject = reject;
        });
    }
    resolve(value) {
        this._isResolved = true;
        this._resolve(value);
    }
    reject(value) {
        this._isResolved = true;
        this._reject(value);
    }
    get isResolved() {
        return this._isResolved;
    }
    get isRejected() {
        return this._isRejected;
    }
    get isFullfilled() {
        return this._isResolved || this._isRejected;
    }
}
exports.Deferred = Deferred;
class AsyncQueue {
    constructor(initializer) {
        this.queue = [];
        this.waiting = [];
        this[Symbol.asyncIterator] = () => {
            return this;
        };
        initializer({
            next: (value) => {
                if (this.waiting.length > 0) {
                    const consumer = this.waiting.shift();
                    if (consumer) {
                        consumer.resolve({
                            done: false,
                            value
                        });
                    }
                }
                else {
                    return this.queue.push({
                        type: 'next',
                        value
                    });
                }
            },
            throw: (error) => {
                if (this.waiting.length > 0) {
                    const consumer = this.waiting.shift();
                    return consumer && consumer.reject(error);
                }
                else {
                    return this.queue.push({
                        value: error,
                        type: 'error'
                    });
                }
            },
            return: (value) => {
                if (this.waiting.length > 0) {
                    const consumer = this.waiting.shift();
                    return consumer && consumer.resolve({
                        done: true,
                        value
                    });
                }
                else {
                    return this.queue.push({
                        value,
                        type: 'return'
                    });
                }
            }
        });
    }
    next() {
        if (this.queue.length > 1) {
            const item = this.queue.shift();
            if (!item) {
                throw new Error('Working around TS strictNullCheck');
            }
            if (item.type === 'return') {
                return Promise.resolve({
                    done: true,
                    value: item.value
                });
            }
            else if (item.type === 'error') {
                return Promise.reject(item.value);
            }
            else {
                return Promise.resolve({
                    done: false,
                    value: item.value
                });
            }
        }
        else {
            const def = new Deferred();
            this.waiting.push(def);
            return def.promise;
        }
    }
}
exports.AsyncQueue = AsyncQueue;

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy91dGlscy9pbmRleC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiOztBQUFNLE1BQU8sQ0FBQyxhQUFhLEdBQUcsTUFBTSxDQUFDLGFBQWEsSUFBSSxNQUFNLENBQUMsR0FBRyxDQUFDLHNCQUFzQixDQUFDLENBQUM7QUFFekY7SUFRSTtRQUhBLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBQ3BCLGdCQUFXLEdBQUcsS0FBSyxDQUFDO1FBR2hCLElBQUksQ0FBQyxPQUFPLEdBQUcsSUFBSSxPQUFPLENBQUMsQ0FBQyxPQUFPLEVBQUUsTUFBTSxFQUFFLEVBQUU7WUFDM0MsSUFBSSxDQUFDLFFBQVEsR0FBRyxPQUFPLENBQUE7WUFDdkIsSUFBSSxDQUFDLE9BQU8sR0FBRyxNQUFNLENBQUE7UUFDekIsQ0FBQyxDQUFDLENBQUE7SUFDTixDQUFDO0lBRU0sT0FBTyxDQUFDLEtBQU87UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLFFBQVEsQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN6QixDQUFDO0lBRU8sTUFBTSxDQUFDLEtBQU87UUFDbEIsSUFBSSxDQUFDLFdBQVcsR0FBRyxJQUFJLENBQUM7UUFDeEIsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLENBQUMsQ0FBQztJQUN4QixDQUFDO0lBRUQsSUFBSSxVQUFVO1FBQ1YsT0FBTyxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQzVCLENBQUM7SUFFRCxJQUFJLFVBQVU7UUFDVixPQUFPLElBQUksQ0FBQyxXQUFXLENBQUM7SUFDNUIsQ0FBQztJQUVELElBQUksWUFBWTtRQUNaLE9BQU8sSUFBSSxDQUFDLFdBQVcsSUFBSSxJQUFJLENBQUMsV0FBVyxDQUFDO0lBQ2hELENBQUM7Q0FDSjtBQXBDRCw0QkFvQ0M7QUFHRDtJQUlJLFlBQVksV0FBcUI7UUFIekIsVUFBSyxHQUF3QyxFQUFFLENBQUM7UUFDaEQsWUFBTyxHQUF1QyxFQUFFLENBQUM7UUFpRnpELEtBQUMsTUFBTSxDQUFDLGFBQWEsQ0FBQyxHQUFHLEdBQUcsRUFBRTtZQUMxQixPQUFPLElBQUksQ0FBQTtRQUNmLENBQUMsQ0FBQTtRQWhGRyxXQUFXLENBQUM7WUFDUixJQUFJLEVBQUUsQ0FBQyxLQUFRLEVBQUUsRUFBRTtnQkFDZixJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtvQkFHekIsTUFBTSxRQUFRLEdBQUcsSUFBSSxDQUFDLE9BQU8sQ0FBQyxLQUFLLEVBQUUsQ0FBQztvQkFDdEMsSUFBSSxRQUFRLEVBQUU7d0JBQ1YsUUFBUSxDQUFDLE9BQU8sQ0FBQzs0QkFDYixJQUFJLEVBQUUsS0FBSzs0QkFDWCxLQUFLO3lCQUNSLENBQUMsQ0FBQTtxQkFDTDtpQkFDSjtxQkFBTTtvQkFDSCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO3dCQUNuQixJQUFJLEVBQUUsTUFBTTt3QkFDWixLQUFLO3FCQUNSLENBQUMsQ0FBQTtpQkFDTDtZQUNMLENBQUM7WUFDRCxLQUFLLEVBQUUsQ0FBQyxLQUFVLEVBQUUsRUFBRTtnQkFDbEIsSUFBSSxJQUFJLENBQUMsT0FBTyxDQUFDLE1BQU0sR0FBRyxDQUFDLEVBQUU7b0JBQ3pCLE1BQU0sUUFBUSxHQUFHLElBQUksQ0FBQyxPQUFPLENBQUMsS0FBSyxFQUFFLENBQUE7b0JBQ3JDLE9BQU8sUUFBUSxJQUFJLFFBQVEsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLENBQUE7aUJBQzVDO3FCQUFNO29CQUNILE9BQU8sSUFBSSxDQUFDLEtBQUssQ0FBQyxJQUFJLENBQUM7d0JBQ25CLEtBQUssRUFBRSxLQUFLO3dCQUNaLElBQUksRUFBRSxPQUFPO3FCQUNoQixDQUFDLENBQUE7aUJBQ0w7WUFDTCxDQUFDO1lBQ0QsTUFBTSxFQUFFLENBQUMsS0FBUSxFQUFFLEVBQUU7Z0JBQ2pCLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO29CQUN6QixNQUFNLFFBQVEsR0FBRyxJQUFJLENBQUMsT0FBTyxDQUFDLEtBQUssRUFBRSxDQUFBO29CQUNyQyxPQUFPLFFBQVEsSUFBSSxRQUFRLENBQUMsT0FBTyxDQUFDO3dCQUNoQyxJQUFJLEVBQUUsSUFBSTt3QkFDVixLQUFLO3FCQUNSLENBQUMsQ0FBQTtpQkFDTDtxQkFBTTtvQkFDSCxPQUFPLElBQUksQ0FBQyxLQUFLLENBQUMsSUFBSSxDQUFDO3dCQUNuQixLQUFLO3dCQUNMLElBQUksRUFBRSxRQUFRO3FCQUNqQixDQUFDLENBQUE7aUJBQ0w7WUFDTCxDQUFDO1NBQ0osQ0FBQyxDQUFBO0lBQ04sQ0FBQztJQUVELElBQUk7UUFDQSxJQUFJLElBQUksQ0FBQyxLQUFLLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUd2QixNQUFNLElBQUksR0FBRyxJQUFJLENBQUMsS0FBSyxDQUFDLEtBQUssRUFBRSxDQUFDO1lBQ2hDLElBQUksQ0FBQyxJQUFJLEVBQUM7Z0JBQ04sTUFBTSxJQUFJLEtBQUssQ0FBQyxtQ0FBbUMsQ0FBQyxDQUFDO2FBQ3hEO1lBQ0QsSUFBSSxJQUFJLENBQUMsSUFBSSxLQUFLLFFBQVEsRUFBRTtnQkFDeEIsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNuQixJQUFJLEVBQUUsSUFBSTtvQkFDVixLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7aUJBQ3BCLENBQUMsQ0FBQTthQUNMO2lCQUFNLElBQUksSUFBSSxDQUFDLElBQUksS0FBSyxPQUFPLEVBQUU7Z0JBQzlCLE9BQU8sT0FBTyxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsS0FBSyxDQUFDLENBQUE7YUFDcEM7aUJBQU07Z0JBQ0gsT0FBTyxPQUFPLENBQUMsT0FBTyxDQUFDO29CQUNuQixJQUFJLEVBQUUsS0FBSztvQkFDWCxLQUFLLEVBQUUsSUFBSSxDQUFDLEtBQUs7aUJBQ3BCLENBQUMsQ0FBQTthQUNMO1NBQ0o7YUFBTTtZQUlILE1BQU0sR0FBRyxHQUFHLElBQUksUUFBUSxFQUFxQixDQUFDO1lBQzlDLElBQUksQ0FBQyxPQUFPLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxDQUFBO1lBQ3RCLE9BQU8sR0FBRyxDQUFDLE9BQU8sQ0FBQztTQUN0QjtJQUNMLENBQUM7Q0FLSjtBQXRGRCxnQ0FzRkMiLCJmaWxlIjoidXRpbHMvaW5kZXguanMiLCJzb3VyY2VzQ29udGVudCI6WyIoPGFueT5TeW1ib2wpLmFzeW5jSXRlcmF0b3IgPSBTeW1ib2wuYXN5bmNJdGVyYXRvciB8fCBTeW1ib2wuZm9yKFwiU3ltYm9sLmFzeW5jSXRlcmF0b3JcIik7XHJcblxyXG5leHBvcnQgY2xhc3MgRGVmZXJyZWQ8VD4ge1xyXG5cclxuICAgIHB1YmxpYyBwcm9taXNlOiBQcm9taXNlPFQ+O1xyXG4gICAgcHVibGljIF9yZXNvbHZlOiBGdW5jdGlvbjtcclxuICAgIHB1YmxpYyBfcmVqZWN0OiBGdW5jdGlvbjtcclxuICAgIF9pc1Jlc29sdmVkID0gZmFsc2U7XHJcbiAgICBfaXNSZWplY3RlZCA9IGZhbHNlO1xyXG5cclxuICAgIGNvbnN0cnVjdG9yKCkge1xyXG4gICAgICAgIHRoaXMucHJvbWlzZSA9IG5ldyBQcm9taXNlKChyZXNvbHZlLCByZWplY3QpID0+IHtcclxuICAgICAgICAgICAgdGhpcy5fcmVzb2x2ZSA9IHJlc29sdmVcclxuICAgICAgICAgICAgdGhpcy5fcmVqZWN0ID0gcmVqZWN0XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBwdWJsaWMgcmVzb2x2ZSh2YWx1ZTpUKXtcclxuICAgICAgICB0aGlzLl9pc1Jlc29sdmVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9yZXNvbHZlKHZhbHVlKTtcclxuICAgIH1cclxuXHJcbiAgICAgcHVibGljIHJlamVjdCh2YWx1ZTpUKXtcclxuICAgICAgICB0aGlzLl9pc1Jlc29sdmVkID0gdHJ1ZTtcclxuICAgICAgICB0aGlzLl9yZWplY3QodmFsdWUpO1xyXG4gICAgfVxyXG5cclxuICAgIGdldCBpc1Jlc29sdmVkKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzUmVzb2x2ZWQ7XHJcbiAgICB9XHJcblxyXG4gICAgZ2V0IGlzUmVqZWN0ZWQoKXtcclxuICAgICAgICByZXR1cm4gdGhpcy5faXNSZWplY3RlZDtcclxuICAgIH1cclxuXHJcbiAgICBnZXQgaXNGdWxsZmlsbGVkKCl7XHJcbiAgICAgICAgcmV0dXJuIHRoaXMuX2lzUmVzb2x2ZWQgfHwgdGhpcy5faXNSZWplY3RlZDtcclxuICAgIH1cclxufVxyXG5cclxuLy9GUk9NOiBodHRwczovL2dpdGh1Yi5jb20vdGMzOS9wcm9wb3NhbC1hc3luYy1pdGVyYXRpb24vaXNzdWVzLzk5XHJcbmV4cG9ydCBjbGFzcyBBc3luY1F1ZXVlPFQ+IHtcclxuICAgIHByaXZhdGUgcXVldWU6IEFycmF5PHsgdHlwZTogc3RyaW5nLCB2YWx1ZTogYW55IH0+ID0gW107XHJcbiAgICBwcml2YXRlIHdhaXRpbmc6IEFycmF5PERlZmVycmVkPEl0ZXJhdG9yUmVzdWx0PFQ+Pj4gPSBbXTtcclxuXHJcbiAgICBjb25zdHJ1Y3Rvcihpbml0aWFsaXplcjogRnVuY3Rpb24pIHtcclxuICAgICAgICBpbml0aWFsaXplcih7XHJcbiAgICAgICAgICAgIG5leHQ6ICh2YWx1ZTogVCkgPT4ge1xyXG4gICAgICAgICAgICAgICAgaWYgKHRoaXMud2FpdGluZy5sZW5ndGggPiAwKSB7XHJcbiAgICAgICAgICAgICAgICAgICAgLy8gSWYgYW55b25lIGlzIHdhaXRpbmcgd2UnbGwganVzdCBzZW5kIHRoZW0gdGhlIHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgLy8gaW1tZWRpYXRlbHlcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb25zdW1lciA9IHRoaXMud2FpdGluZy5zaGlmdCgpO1xyXG4gICAgICAgICAgICAgICAgICAgIGlmIChjb25zdW1lcikge1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBjb25zdW1lci5yZXNvbHZlKHtcclxuICAgICAgICAgICAgICAgICAgICAgICAgICAgIGRvbmU6IGZhbHNlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICAgICAgdmFsdWVcclxuICAgICAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnF1ZXVlLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB0eXBlOiAnbmV4dCcsXHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlXHJcbiAgICAgICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgICAgIH1cclxuICAgICAgICAgICAgfSxcclxuICAgICAgICAgICAgdGhyb3c6IChlcnJvcjogYW55KSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy53YWl0aW5nLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb25zdW1lciA9IHRoaXMud2FpdGluZy5zaGlmdCgpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbnN1bWVyICYmIGNvbnN1bWVyLnJlamVjdChlcnJvcilcclxuICAgICAgICAgICAgICAgIH0gZWxzZSB7XHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIHRoaXMucXVldWUucHVzaCh7XHJcbiAgICAgICAgICAgICAgICAgICAgICAgIHZhbHVlOiBlcnJvcixcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ2Vycm9yJ1xyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9XHJcbiAgICAgICAgICAgIH0sXHJcbiAgICAgICAgICAgIHJldHVybjogKHZhbHVlOiBUKSA9PiB7XHJcbiAgICAgICAgICAgICAgICBpZiAodGhpcy53YWl0aW5nLmxlbmd0aCA+IDApIHtcclxuICAgICAgICAgICAgICAgICAgICBjb25zdCBjb25zdW1lciA9IHRoaXMud2FpdGluZy5zaGlmdCgpXHJcbiAgICAgICAgICAgICAgICAgICAgcmV0dXJuIGNvbnN1bWVyICYmIGNvbnN1bWVyLnJlc29sdmUoe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICBkb25lOiB0cnVlLFxyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZVxyXG4gICAgICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgICAgIHJldHVybiB0aGlzLnF1ZXVlLnB1c2goe1xyXG4gICAgICAgICAgICAgICAgICAgICAgICB2YWx1ZSxcclxuICAgICAgICAgICAgICAgICAgICAgICAgdHlwZTogJ3JldHVybidcclxuICAgICAgICAgICAgICAgICAgICB9KVxyXG4gICAgICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICB9XHJcbiAgICAgICAgfSlcclxuICAgIH1cclxuXHJcbiAgICBuZXh0KCk6IFByb21pc2U8SXRlcmF0b3JSZXN1bHQ8VD4+e1xyXG4gICAgICAgIGlmICh0aGlzLnF1ZXVlLmxlbmd0aCA+IDEpIHtcclxuICAgICAgICAgICAgLy8gSWYgdGhlcmUgYXJlIGl0ZW1zIGF2YWlsYWJsZSB0aGVuIHNpbXBseSBwdXQgdGhlbVxyXG4gICAgICAgICAgICAvLyBpbnRvIHRoZSBxdWV1ZVxyXG4gICAgICAgICAgICBjb25zdCBpdGVtID0gdGhpcy5xdWV1ZS5zaGlmdCgpO1xyXG4gICAgICAgICAgICBpZiAoIWl0ZW0pe1xyXG4gICAgICAgICAgICAgICAgdGhyb3cgbmV3IEVycm9yKCdXb3JraW5nIGFyb3VuZCBUUyBzdHJpY3ROdWxsQ2hlY2snKTtcclxuICAgICAgICAgICAgfVxyXG4gICAgICAgICAgICBpZiAoaXRlbS50eXBlID09PSAncmV0dXJuJykge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZG9uZTogdHJ1ZSxcclxuICAgICAgICAgICAgICAgICAgICB2YWx1ZTogaXRlbS52YWx1ZVxyXG4gICAgICAgICAgICAgICAgfSlcclxuICAgICAgICAgICAgfSBlbHNlIGlmIChpdGVtLnR5cGUgPT09ICdlcnJvcicpIHtcclxuICAgICAgICAgICAgICAgIHJldHVybiBQcm9taXNlLnJlamVjdChpdGVtLnZhbHVlKVxyXG4gICAgICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAgICAgcmV0dXJuIFByb21pc2UucmVzb2x2ZSh7XHJcbiAgICAgICAgICAgICAgICAgICAgZG9uZTogZmFsc2UsXHJcbiAgICAgICAgICAgICAgICAgICAgdmFsdWU6IGl0ZW0udmFsdWVcclxuICAgICAgICAgICAgICAgIH0pXHJcbiAgICAgICAgICAgIH1cclxuICAgICAgICB9IGVsc2Uge1xyXG4gICAgICAgICAgICAvLyBJZiB0aGVyZSdzIG5vdGhpbmcgYXZhaWxhYmxlIHRoZW4gc2ltcGx5XHJcbiAgICAgICAgICAgIC8vIGdpdmUgYmFjayBhIFByb21pc2UgaW1tZWRpYXRlbHkgZm9yIHdoZW4gYSB2YWx1ZSBldmVudHVhbGx5XHJcbiAgICAgICAgICAgIC8vIGNvbWVzIGluXHJcbiAgICAgICAgICAgIGNvbnN0IGRlZiA9IG5ldyBEZWZlcnJlZDxJdGVyYXRvclJlc3VsdDxUPj4oKTtcclxuICAgICAgICAgICAgdGhpcy53YWl0aW5nLnB1c2goZGVmKVxyXG4gICAgICAgICAgICByZXR1cm4gZGVmLnByb21pc2U7XHJcbiAgICAgICAgfVxyXG4gICAgfVxyXG5cclxuICAgIFtTeW1ib2wuYXN5bmNJdGVyYXRvcl0gPSAoKSA9PiAge1xyXG4gICAgICAgIHJldHVybiB0aGlzXHJcbiAgICB9XHJcbn0iXX0=
