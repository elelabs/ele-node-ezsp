"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const application_1 = require("./application");
const named_1 = require("./types/named");
const application = new application_1.ControllerApplication();
application.on('incomingMessage', ({ apsFrame, sender, senderEui64, message }) => {
    console.log('incomingMessage', sender, senderEui64 && senderEui64.toString(), apsFrame, message);
});
application.startup('/dev/ttyUSB1', {
    baudRate: 57600,
    parity: 'none',
    stopBits: 1,
    xon: true,
    xoff: true
}, {}, {}).then(() => __awaiter(this, void 0, void 0, function* () {
    let localEui64 = yield application.getLocalEUI64();
    console.log('Local Eui64:', localEui64.toString());
    var res = yield application.request(0xA329, {
        clusterId: 0x11, profileId: 0xC105,
        sequence: 1,
        sourceEndpoint: 0xE8, destinationEndpoint: 0xE8,
        options: named_1.EmberApsOption.APS_OPTION_FORCE_ROUTE_DISCOVERY | named_1.EmberApsOption.APS_OPTION_RETRY
    }, Buffer.from('\nTESTING!\n'), 0);
    console.log('Sent=', res);
    console.log('nodeID:', (yield application.networkIdToEUI64(41769)).toString());
    var nwkParams = yield application.getNetworkParameters();
    console.log(nwkParams);
}));

//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi4uL3NyYy90ZXN0LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7Ozs7Ozs7Ozs7QUFBQSwrQ0FBc0Q7QUFFdEQseUNBQTJEO0FBRTNELE1BQU0sV0FBVyxHQUFHLElBQUksbUNBQXFCLEVBQUUsQ0FBQztBQUVoRCxXQUFXLENBQUMsRUFBRSxDQUFDLGlCQUFpQixFQUFFLENBQUMsRUFBRSxRQUFRLEVBQUUsTUFBTSxFQUFFLFdBQVcsRUFBRyxPQUFPLEVBQTBGLEVBQUUsRUFBRTtJQUN4SyxPQUFPLENBQUMsR0FBRyxDQUFDLGlCQUFpQixFQUFFLE1BQU0sRUFBRSxXQUFXLElBQUksV0FBVyxDQUFDLFFBQVEsRUFBRSxFQUFFLFFBQVEsRUFBRSxPQUFPLENBQUMsQ0FBQztBQUNuRyxDQUFDLENBQUMsQ0FBQztBQUVILFdBQVcsQ0FBQyxPQUFPLENBQUMsY0FBYyxFQUFFO0lBQ2xDLFFBQVEsRUFBRSxLQUFLO0lBQ2YsTUFBTSxFQUFFLE1BQU07SUFDZCxRQUFRLEVBQUUsQ0FBQztJQUNYLEdBQUcsRUFBRSxJQUFJO0lBQ1QsSUFBSSxFQUFFLElBQUk7Q0FDWCxFQUFFLEVBQUUsRUFBRSxFQUFFLENBQUMsQ0FBQyxJQUFJLENBQUMsR0FBUyxFQUFFO0lBRXpCLElBQUksVUFBVSxHQUFHLE1BQU0sV0FBVyxDQUFDLGFBQWEsRUFBRSxDQUFDO0lBQ25ELE9BQU8sQ0FBQyxHQUFHLENBQUMsY0FBYyxFQUFFLFVBQVUsQ0FBQyxRQUFRLEVBQUUsQ0FBQyxDQUFDO0lBRW5ELElBQUksR0FBRyxHQUFHLE1BQU0sV0FBVyxDQUFDLE9BQU8sQ0FBQyxNQUFNLEVBQUU7UUFDMUMsU0FBUyxFQUFFLElBQUksRUFBRSxTQUFTLEVBQUUsTUFBTTtRQUNsQyxRQUFRLEVBQUUsQ0FBQztRQUNYLGNBQWMsRUFBRSxJQUFJLEVBQUUsbUJBQW1CLEVBQUUsSUFBSTtRQUMvQyxPQUFPLEVBQUUsc0JBQWMsQ0FBQyxnQ0FBZ0MsR0FBRyxzQkFBYyxDQUFDLGdCQUFnQjtLQUMzRixFQUFFLE1BQU0sQ0FBQyxJQUFJLENBQUMsY0FBYyxDQUFDLEVBQUUsQ0FBQyxDQUFDLENBQUM7SUFFbkMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxPQUFPLEVBQUUsR0FBRyxDQUFDLENBQUM7SUFFekIsT0FBTyxDQUFDLEdBQUcsQ0FBQyxTQUFTLEVBQUUsQ0FBQyxNQUFNLFdBQVcsQ0FBQyxnQkFBZ0IsQ0FBQyxLQUFLLENBQUMsQ0FBQyxDQUFDLFFBQVEsRUFBRSxDQUFDLENBQUM7SUFFaEYsSUFBSSxTQUFTLEdBQUcsTUFBTSxXQUFXLENBQUMsb0JBQW9CLEVBQUUsQ0FBQztJQUN6RCxPQUFPLENBQUMsR0FBRyxDQUFDLFNBQVMsQ0FBQyxDQUFDO0FBQ3pCLENBQUMsQ0FBQSxDQUFDLENBQUMiLCJmaWxlIjoidGVzdC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbnRyb2xsZXJBcHBsaWNhdGlvbiB9IGZyb20gJy4vYXBwbGljYXRpb24nO1xyXG5pbXBvcnQgeyBFbWJlckFwc0ZyYW1lIH0gZnJvbSAnLi90eXBlcy9zdHJ1Y3QnO1xyXG5pbXBvcnQgeyBFbWJlckFwc09wdGlvbiwgRW1iZXJFVUk2NCB9IGZyb20gJy4vdHlwZXMvbmFtZWQnO1xyXG5cclxuY29uc3QgYXBwbGljYXRpb24gPSBuZXcgQ29udHJvbGxlckFwcGxpY2F0aW9uKCk7XHJcblxyXG5hcHBsaWNhdGlvbi5vbignaW5jb21pbmdNZXNzYWdlJywgKHsgYXBzRnJhbWUsIHNlbmRlciwgc2VuZGVyRXVpNjQsICBtZXNzYWdlIH06IHsgYXBzRnJhbWU6IEVtYmVyQXBzRnJhbWUsIHNlbmRlcjogbnVtYmVyLCBzZW5kZXJFdWk2NDogRW1iZXJFVUk2NCwgIG1lc3NhZ2U6IEJ1ZmZlciB9KSA9PiB7XHJcbiAgY29uc29sZS5sb2coJ2luY29taW5nTWVzc2FnZScsIHNlbmRlciwgc2VuZGVyRXVpNjQgJiYgc2VuZGVyRXVpNjQudG9TdHJpbmcoKSwgYXBzRnJhbWUsIG1lc3NhZ2UpO1xyXG59KTtcclxuXHJcbmFwcGxpY2F0aW9uLnN0YXJ0dXAoJy9kZXYvdHR5VVNCMScsIHtcclxuICBiYXVkUmF0ZTogNTc2MDAsXHJcbiAgcGFyaXR5OiAnbm9uZScsXHJcbiAgc3RvcEJpdHM6IDEsXHJcbiAgeG9uOiB0cnVlLFxyXG4gIHhvZmY6IHRydWVcclxufSwge30sIHt9KS50aGVuKGFzeW5jICgpID0+IHtcclxuXHJcbiAgbGV0IGxvY2FsRXVpNjQgPSBhd2FpdCBhcHBsaWNhdGlvbi5nZXRMb2NhbEVVSTY0KCk7XHJcbiAgY29uc29sZS5sb2coJ0xvY2FsIEV1aTY0OicsIGxvY2FsRXVpNjQudG9TdHJpbmcoKSk7XHJcblxyXG4gIHZhciByZXMgPSBhd2FpdCBhcHBsaWNhdGlvbi5yZXF1ZXN0KDB4QTMyOSwge1xyXG4gICAgY2x1c3RlcklkOiAweDExLCBwcm9maWxlSWQ6IDB4QzEwNSxcclxuICAgIHNlcXVlbmNlOiAxLFxyXG4gICAgc291cmNlRW5kcG9pbnQ6IDB4RTgsIGRlc3RpbmF0aW9uRW5kcG9pbnQ6IDB4RTgsXHJcbiAgICBvcHRpb25zOiBFbWJlckFwc09wdGlvbi5BUFNfT1BUSU9OX0ZPUkNFX1JPVVRFX0RJU0NPVkVSWSB8IEVtYmVyQXBzT3B0aW9uLkFQU19PUFRJT05fUkVUUllcclxuICB9LCBCdWZmZXIuZnJvbSgnXFxuVEVTVElORyFcXG4nKSwgMCk7XHJcblxyXG4gIGNvbnNvbGUubG9nKCdTZW50PScsIHJlcyk7XHJcblxyXG4gICBjb25zb2xlLmxvZygnbm9kZUlEOicsIChhd2FpdCBhcHBsaWNhdGlvbi5uZXR3b3JrSWRUb0VVSTY0KDQxNzY5KSkudG9TdHJpbmcoKSk7IFxyXG5cclxuICB2YXIgbndrUGFyYW1zID0gYXdhaXQgYXBwbGljYXRpb24uZ2V0TmV0d29ya1BhcmFtZXRlcnMoKTtcclxuICBjb25zb2xlLmxvZyhud2tQYXJhbXMpO1xyXG59KTtcclxuXHJcblxyXG4iXX0=
