/// <reference types="node" />
import * as basic from './basic';
import * as named from './named';
export declare class EzspStruct {
    constructor(...args: any[]);
    static serialize(cls: any, obj: any): Buffer;
    static deserialize(cls: any, data: Buffer): any[];
}
export declare class EmberNetworkParameters extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberZigbeeNetwork extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberApsFrame extends EzspStruct {
    profileId: number;
    sequence: number;
    clusterId: number;
    sourceEndpoint: number;
    destinationEndpoint: number;
    groupId?: number;
    options?: named.EmberApsOption;
    static _fields: (string | typeof basic.uint16_t)[][];
}
export declare class EmberBindingTableEntry extends EzspStruct {
    static _fields: ((string | typeof basic.uint16_t)[] | (string | typeof named.EmberEUI64)[])[];
}
export declare class EmberMulticastTableEntry extends EzspStruct {
    static _fields: (string | typeof basic.uint8_t)[][];
}
export declare class EmberKeyData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberCertificateData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberPublicKeyData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberPrivateKeyData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberSmacData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberSignatureData extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberCertificate283k1Data extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberPublicKey283k1Data extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberPrivateKey283k1Data extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberSignature283k1Data extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberMessageDigest extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberAesMmoHashContext extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberNeighborTableEntry extends EzspStruct {
    static _fields: ((string | typeof basic.uint16_t)[] | (string | typeof named.EmberEUI64)[])[];
}
export declare class EmberRouteTableEntry extends EzspStruct {
    static _fields: (string | typeof basic.uint16_t)[][];
}
export declare class EmberInitialSecurityState extends EzspStruct {
    static _fields: ((string | typeof basic.uint8_t)[] | (string | typeof named.EmberEUI64)[] | (string | typeof EmberKeyData)[])[];
}
export declare class EmberCurrentSecurityState extends EzspStruct {
    static _fields: ((string | typeof named.EmberEUI64)[] | (string | typeof named.EmberCurrentSecurityBitmask)[])[];
}
export declare class EmberKeyStruct extends EzspStruct {
    static _fields: ((string | typeof basic.uint8_t)[] | (string | typeof named.EmberEUI64)[] | (string | typeof EmberKeyData)[])[];
}
export declare class EmberNetworkInitStruct extends EzspStruct {
    static _fields: (string | typeof named.EmberNetworkInitBitmask)[][];
}
export declare class EmberZllSecurityAlgorithmData extends EzspStruct {
    static _fields: (string | typeof basic.uint16_t)[][];
}
export declare class EmberZllNetwork extends EzspStruct {
    static _fields: ((string | typeof basic.uint8_t)[] | (string | typeof named.EmberEUI64)[] | (string | typeof EmberZigbeeNetwork)[])[];
}
export declare class EmberZllInitialSecurityState extends EzspStruct {
    static _fields: ((string | typeof basic.uint32_t)[] | (string | typeof EmberKeyData)[])[];
}
export declare class EmberZllDeviceInfoRecord extends EzspStruct {
    static _fields: ((string | typeof basic.uint16_t)[] | (string | typeof named.EmberEUI64)[])[];
}
export declare class EmberZllAddressAssignment extends EzspStruct {
    static _fields: (string | typeof named.EmberNodeId)[][];
}
export declare class EmberTokTypeStackZllData extends EzspStruct {
    static _fields: (string | typeof basic.uint16_t)[][];
}
export declare class EmberTokTypeStackZllSecurity extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberRf4ceVendorInfo extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberRf4ceApplicationInfo extends EzspStruct {
    static _fields: (string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[][];
}
export declare class EmberRf4cePairingTableEntry extends EzspStruct {
    static _fields: ((string | {
        new (): any;
        deserialize(cls: any, data: Buffer): any;
    })[] | (string | typeof named.EmberEUI64)[])[];
}
export declare class EmberGpAddress extends EzspStruct {
    static _fields: ((string | typeof basic.uint8_t)[] | (string | typeof named.EmberEUI64)[])[];
}
export declare class EmberGpSinkListEntry extends EzspStruct {
    static _fields: ((string | typeof basic.uint8_t)[] | (string | typeof named.EmberEUI64)[])[];
}
