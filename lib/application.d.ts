/// <reference types="node" />
import { EventEmitter } from "events";
import { EmberApsFrame, EmberNetworkParameters } from './types/struct';
import { EmberEUI64 } from './types/named';
export declare class ControllerApplication extends EventEmitter {
    private direct;
    private ezsp;
    private eui64ToNodeId;
    private pending;
    constructor(nodeInfo?: Iterable<{
        nodeId: number;
        eui64: string | EmberEUI64;
    }>);
    startup(port: string, options: {}, network_parameters: {}, security_parameters: {}): Promise<any>;
    private handleFrame(frameName, ...args);
    private handleNodeLeft(nwk, ieee, ...args);
    private handleNodeJoined(nwk, ieee, deviceUpdate, joinDec, parentNwk);
    private handleReply(sender, apsFrame, tsn, commandId, ...args);
    request(nwk: number | EmberEUI64, apsFrame: EmberApsFrame, data: Buffer, timeout?: number): Promise<boolean>;
    private handleFrameFailure(messageType, destination, apsFrame, messageTag, status, message);
    private handleFrameSent(messageType, destination, apsFrame, messageTag, status, message);
    stop(): any;
    getLocalEUI64(): Promise<EmberEUI64>;
    networkIdToEUI64(nwk: number): Promise<EmberEUI64>;
    permitJoining(seconds: number): Promise<Buffer>;
    getNetworkParameters(): Promise<{
        nodeType: number;
        networkParams: EmberNetworkParameters;
    }>;
}
