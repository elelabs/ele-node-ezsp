/// <reference types="node" />
import { UartProtocol } from './uart';
import { Deferred } from './utils';
import { EmberOutgoingMessageType } from './types/named';
import { EventEmitter } from 'events';
import { EmberApsFrame } from './types/struct';
export declare class Ezsp extends EventEmitter {
    ezsp_version: number;
    _gw: UartProtocol;
    _port: any;
    _seq: number;
    _awaiting: Map<number, {
        expectedId: number;
        schema: any;
        deferred: Deferred<Buffer>;
    }>;
    COMMANDS_BY_ID: Map<number, {
        name: string;
        inArgs: any[];
        outArgs: any[];
    }>;
    _cbCounter: number;
    constructor();
    connect(device: string, options: {}): Promise<void>;
    private startReadQueue();
    reset(): Promise<any>;
    version(): Promise<number>;
    networkInit(): Promise<boolean>;
    setConfigurationValue(configId: number, value: any): Promise<void>;
    close(): any;
    private _ezsp_frame(name, ...args);
    private _command(name, ...args);
    formNetwork(parameters: {}): Promise<Buffer>;
    execCommand(name: string, ...args: any[]): Promise<Buffer>;
    frame_received(data: Buffer): void;
    sendUnicast(direct: EmberOutgoingMessageType, nwk: number, apsFrame: EmberApsFrame, seq: number, data: Buffer): Promise<Buffer>;
}
